﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActumTest
{
    /// <summary>
    /// Abstract base class for every PageObject class. It contains currently only WebDriver reference. 
    /// It's used mainly for encapsulating logic common for all Page Objects.
    /// </summary>
    public abstract class SeleniumPageObject
    {       
        public IWebDriver Driver { get; private set; }
        
        public SeleniumPageObject(IWebDriver driver)
        {
            this.Driver = driver;
        }       
    }
}
