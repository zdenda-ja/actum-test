﻿using OpenQA.Selenium;
using System.Threading;

namespace ActumTest
{
    /// <summary>
    /// Extension methods for IWebElement class
    /// </summary>
    public static class IWebElementExtensions
    {
        /// <summary>
        /// This method uses Thread.Sleep as a solution for IWebElement.Click() reliability issues. 
        /// </summary>
        /// <param name="element"></param>
        public static void ReliableClick(this IWebElement element)
        {
            Thread.Sleep(500);
            element.Click();
        }
    }
    
}
