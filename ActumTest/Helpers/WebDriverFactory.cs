﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using OpenQA.Selenium;

namespace ActumTest
{
    /// <summary>
    /// This is helper class for encapsulating work with Web drivers and act as WebDrivers instance factory.
    /// Here should be logic for default settings, instanciation, singleton and common logic.
    /// Currently only Chrome and Firefox drivers are supported. 
    /// </summary>
    public class WebDriverFactory
    {
        /// <summary>
        /// Singleton instance of WebDriver used while tests are running. 
        /// </summary>
        public static IWebDriver Instance { get; set; }

        /// <summary>
        /// Create instance of WebDriver based on generics parameter.
        /// </summary>
        /// <typeparam name="TWebDriver"></typeparam>
        public static void GetDriver<TWebDriver>() where TWebDriver : IWebDriver, new()
        {
            if (typeof(TWebDriver) == typeof(ChromeDriver))
            {
                Instance = new ChromeDriver();
            }
            else if (typeof(TWebDriver) == typeof(FirefoxDriver))
            {
                Instance = new FirefoxDriver();
            }

            if (Instance != null)
            {
                Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            }
            else
            {
                throw new InvalidOperationException("Unsupported WebDriver. Only ChromeDriver and FirefoxDriver are supported.");
            }
        }
    }
}
