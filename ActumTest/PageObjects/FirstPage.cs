﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace ActumTest
{
    /// <summary>
    /// PageObject for first page of google's form. 
    /// Test operator doesn't handle particular form fields directly but only through public methods. 
    /// Form fields are not publicly visible and are for private use only. 
    /// </summary>
    public class FirstPage : SeleniumPageObject
    {
        /// <summary>
        /// As PageObject refers mostly specific Url, it make sense to incorporate it here as static property.
        /// Static because url is required before instance of this class is created and initiated by PageFactory.
        /// </summary>
        public static string Url
        {
            get
            {
                return "https://docs.google.com/forms/d/e/1FAIpQLSeY_W-zSf2_JxR4drhbgIxdEwdbUbE4wXhxHZLgxZGiMcNl7g/viewform";
            }
        }
        
        // This region contains private form fields. 
        //All are located by XPath locators mentioned in FindsBy attributes.
        #region Private form fields
        [FindsBy(How = How.XPath, Using = "//div[@jsname='OCpkoe']")]
        private IWebElement NextButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='FkQz1b']")]
        private IList<IWebElement> FirstQuestionCheckboxes { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='jPalo']//input")]
        private IWebElement Date { get; set; }
        
        [FindsBy(How = How.XPath, Using = "//div[@jsname='W85ice']//input")]
        private IWebElement TextQuestion { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='ibnC6b' and contains(@class,'HasError')]")]
        private IWebElement ErrorOnTextQuestion { get; set; }
        #endregion

        public FirstPage(IWebDriver driver) : base(driver)
        {
        }

        //This region contains public methods which may be used to 
        //operate corresponding form (page).
        #region Public methods

        public bool HasErrorOnTextQuestion()
        {
            try
            {
                var size = this.ErrorOnTextQuestion.Size;
                return true;
            }
            catch(NoSuchElementException)
            {
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Checkbox(int number)
        {
            var boxes = this.FirstQuestionCheckboxes;

            if (number > boxes.Count || number < 1)
            {
                throw new InvalidOperationException("Test definition error: invalid checkbox index.");
            }

            boxes[number - 1].ReliableClick();     
        }

        public void EnterDate(DateTime date)
        {
            this.Date.SendKeys("05.05.2017");
        }

        public string GetTextQuestionText()
        {
            return this.TextQuestion.GetAttribute("value");
        }

        public void EnterTextQuestion(string text)
        {
            this.TextQuestion.Clear();
            this.TextQuestion.SendKeys(text);
        }

        public void Next()
        {   
            this.NextButton.ReliableClick();
        }


        #endregion
    }
}
