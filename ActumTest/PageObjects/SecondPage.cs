﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;

namespace ActumTest
{
    /// <summary>
    /// PageObject for second page of google's form. 
    /// Test operator doesn't handle particular form fields directly but only through public methods. 
    /// Form fields are not publicly visible and are for private use only. 
    /// </summary>
    public class SecondPage : SeleniumPageObject
    {
        // This region contains private form fields. 
        //All are located by XPath locators mentioned in FindsBy attributes.
        #region Private form fields

        [FindsBy(How = How.XPath, Using = "//textarea[@jsname='YPqjbf']")]
        private IWebElement TextQuestion { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='LgbsSe']")]
        private IWebElement ColorCombo { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='V68bde']//div[@jsname='wQNmvb']")]
        private IList<IWebElement> ColorPopup { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='wQNmvb']")]
        private IList<IWebElement> SelectedColors { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='GeGHKb']")]
        private IWebElement BackButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='OCpkoe']")]
        private IWebElement NextButton { get; set; }



        #endregion

        public SecondPage(IWebDriver driver) : base(driver)
        {
        }

        //This region contains public methods which may be used to 
        //operate corresponding form (page).
        #region Public methods

        public void EnterTextQuestion(string text)
        {
            this.TextQuestion.Clear();
            this.TextQuestion.SendKeys(text);
        }

        public string GetTextQuestionText()
        {
            return this.TextQuestion.GetAttribute("value");
        }

        public string GetColor()
        {
            var element = this.SelectedColors.SingleOrDefault(s => s.GetAttribute("aria-selected") == "true" && !string.IsNullOrWhiteSpace(s.GetAttribute("data-value")));
            if (element == null) return string.Empty;

            return element.GetAttribute("data-value");
        }

        public void PickRandomColor()
        {
            this.ColorCombo.ReliableClick();
            this.ColorPopup[1].ReliableClick();
        }

        public void Back()
        {
            this.BackButton.ReliableClick();
        }

        public void Next()
        {
            this.NextButton.ReliableClick();
        }

        #endregion

    }
}
