﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace ActumTest
{
    /// <summary>
    /// PageObject for third page of google's form. 
    /// Test operator doesn't handle particular form fields directly but only through public methods. 
    /// Form fields are not publicly visible and are for private use only. 
    /// </summary>
    public class ThirdPage : SeleniumPageObject
    {
        // This region contains private form fields. 
        //All are located by XPath locators mentioned in FindsBy attributes.
        #region Private form fields

        [FindsBy(How = How.XPath, Using = "//div[@jscontroller='EcW08c']")]
        private IList<IWebElement> FirstQuestionRadios { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@jsname='M2UYVd']")]
        private IWebElement SubmitButton { get; set; }

        #endregion

        public ThirdPage(IWebDriver driver) : base(driver)
        {
        }

        //This region contains public methods which may be used to 
        //operate corresponding form (page).
        #region Public methods

        public void CheckRadio(int number)
        {
            var radios = this.FirstQuestionRadios;

            if (number > radios.Count || number < 1)
            {
                throw new InvalidOperationException("Test definition error: invalid radio index.");
            }

            radios[number - 1].ReliableClick();
        }

        public void Submit()
        {
            this.SubmitButton.ReliableClick();
        }

        public bool IsFormSubmittedIn3Seconds()
        {
            try
            {
                var wait = new WebDriverWait(WebDriverFactory.Instance, TimeSpan.FromSeconds(3));
                wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(@"//div[@class='freebirdFormviewerViewResponseConfirmationMessage']")));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        #endregion

    }
}
