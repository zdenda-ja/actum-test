﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Linq;

// master change
// test1 branch
// confliting change in master branch 12
// change in test branch fdsa fads
namespace ActumTest
{
    /// <summary>
    /// NUnit test class containing implementation of Actum Google Form test. 
    /// There are multiple test fixtures ready for Chrome and Firefox browsers. 
    /// As Firefox is problematic for automation it is now disabled. Once TestFixture attribute for 
    /// Firefox is uncommented, it will mutliply tests and run then with different WebDriver.
    /// Type Parameter from TestFixture attribute is passed to class as generic type and is further
    /// used for creating instance of correct IWebDriver type.
    /// </summary>
    /// <typeparam name="TWebDriver"></typeparam>
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(FirefoxDriver))]
    public class GooleFormTests<TWebDriver> where TWebDriver : IWebDriver, new()
    {
        /// <summary>
        /// Setup method is called by NUnit framework before every test method is invoked.
        /// It is usually used to initialize web driver to singleton instance and do some other 
        /// preparations for test environment.
        /// </summary>
        [SetUp]
        public void Init()
        {
            WebDriverFactory.GetDriver<TWebDriver>();
        }

        /// <summary>
        /// Test method implementing Actum Google Form test. It uses PageFactory to instanciate and initialize
        /// PageObjects. Test is implemented by calling public methods exposed by particular PageObjects 
        /// (Google Form pages). 
        /// </summary>
        [Test]
        public void Test()
        {
            //set url of form
            WebDriverFactory.Instance.Url = FirstPage.Url;
            
            // get page object using active web driver
            var page1 = PageFactory.InitElements<FirstPage>(WebDriverFactory.Instance);
            
            // 1) Fill first and second question
            page1.Checkbox(1);
            page1.Checkbox(2);
            page1.EnterDate(DateTime.Now.AddDays(3));

            // 2) Validate that third question is mandatory
            page1.Next();
            Assert.True(page1.HasErrorOnTextQuestion(), "Expected validation on text question didn't fire.");

            //3) Fill third question and go to another step
            page1.EnterTextQuestion("abcdef");
            page1.Next();

            // page is changed
            var page2 = PageFactory.InitElements<SecondPage>(WebDriverFactory.Instance);

            // 4) Fill next questions
            page2.EnterTextQuestion("random");
            page2.PickRandomColor();

            // 5) Go back to first step
            page2.Back();

            // 6) Reverse text in third question
            // page is changed
            page1 = PageFactory.InitElements<FirstPage>(WebDriverFactory.Instance);
            Assert.That(string.IsNullOrWhiteSpace(page1.GetTextQuestionText()), Is.False, "Text question on page1 is empty after page transition.");
            page1.EnterTextQuestion(string.Concat(page1.GetTextQuestionText().Reverse()));

            // 7) Go to second step
            page1.Next();

            // 8) Check that both questions are still filed
            // page is changed
            page2 = PageFactory.InitElements<SecondPage>(WebDriverFactory.Instance);
            Assert.That(string.IsNullOrWhiteSpace(page2.GetTextQuestionText()), Is.False, "Text question on page2 is empty after page transition.");
            Assert.That(string.IsNullOrWhiteSpace(page2.GetColor()), Is.False, "Color on page2 is empty after page transition.");

            // 9) Go to last step
            page2.Next();

            // 10) Fill last question and send form
            var page3 = PageFactory.InitElements<ThirdPage>(WebDriverFactory.Instance);
            page3.CheckRadio(1);
            page3.Submit();
            Assert.True(page3.IsFormSubmittedIn3Seconds(), "Client didn't received form's submit response until 3 seconds");
        }

        // This method is called once test is done. It is usually used for garbage collecting.
        [TearDown]
        public void Clean()
        {
            WebDriverFactory.Instance.Quit();
        }
    }

 

    

    



}
