﻿# Actum Google Form test implementation

This project contains test implementation for Actum Google form test on this [location](https://goo.gl/forms/M9aJeTFWC2Gl1Zap1).

It is implemented in .NET, using C# language, Selenium WebDriver and NUnit framework. It was tested in Visual Studio 2015 with NUnit Adapter extension but could be run in any interactive NUnit-compatible test runner.

As it wasn't part of the examination, this guide doesn't include full installation instructions, only implementation details.

## Implementation details

* Test assembly utilizes ChromeDriver and FirefoxDriver classes which talks directly and locally to browsers. Hence this project **must be run on interactive desktop**.  
* **PageFactory + PageObjects** were used to encapsulate work with HTML pages. PageObject only exposes public method to operate particular form page. Developer who implements the test doesn't deal with HTML form implementation details and focus only on test definition from "business" point of view.
* As Google Forms doesn't offer any standard fixed form identificators as Name, Id or specific Class names, it was observed that attribute **"jsname"** is fixed and stay same through user sessions. This attribute was used as main **XPath selector** for building PageObjects. This solution may not be ideal and this decision **should be discussed properly with form developers** in real world scenario. 
* Test could be run in **multiple browsers** without necessity for test reimplementation. This is behavior is implemented using multiple TestFixture attributes and generics WebDriver factory. Please see source code comments for more details. 
* Some form fields are HTML5 only and test would have to be slightly altered for browser not properly supporting HTML5 tags.  
* WebDriver timeout is set to 5 seconds, which requires decent internet connection and web responses.
* Selenium's IWebElement.Click() method is not always reliable. If there's repeatable calling of Click() it sometimes didn't perform click without any exception. This issue is described [here](https://github.com/SeleniumHQ/selenium/issues/4075). There are several workarounds, I've implemented extension method ReliableClick() for IWebElement class where I deal with that issue by Thread.Sleep. Based on my experience this is most reliable solution with little additional costs.  

## Room for improvements

* public methods of PageObjects such as EnterDate(DateTime) or EnterTextQuestion(string) could be **enhanced to handle NoSuchElementException using aspects programming and tools such PostSharp**. This would make structural-based test failures more friendly for reading. Aspect programming allows to handle exceptions without try/catch blocks and would help to keep code clean. See [this article](http://doc.postsharp.net/exception-handling) for further reference.
* Solution could be implemented without neccessity for interactive desktop by using RemoteWebDriver or headless browser.
* Some more source code comments.

## Authors

* **Zdenek Jirus** - *Initial work* 


